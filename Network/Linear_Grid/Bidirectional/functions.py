# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 07:16:32 2016

@author: arindam
"""
import numpy as np

def maxima(x,t,par):
    """Detects maximas in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas as the second column and the time instances when those maximas
        occur as the third column. If no maximas found, returns a [par, 0, 0]
        array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    m=np.array([par,0,0])
    for i in range(1,len(x)-1):
        if x[i]>x[i+1] and x[i]>x[i-1]:
            m=np.vstack((m,np.array([par,x[i],t[i]])))
    if np.shape(m)==(3,):
        print("No maximas detected")
    else:
        m=m[1:,:]
    return m

def spikes(x,t,par,threshold=0.9):
    """Detects maximas above a set threshold (spikes) in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.
        threshold       : Theshold above which the maximas are defined spikes.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas above the threshold as the second column and the time instances
        when those maximas occur as the third column. If no valid maximas found,
        returns a [par, 0, 0] array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    e=np.array([par,0,0])
    maxs=maxima(x,t,par)

    if np.shape(maxs)==(3,):
        print("No Spikes detected")
        return e

    for i in range(len(maxs)):
        if maxs[i,1]>threshold:
            e=np.vstack((e,maxs[i,:]))

    if np.shape(e)==(3,):
        print("No Spikes detected")
    else:
        e=e[1:,:]
    return e

def isi(x,t,par):
    """Computes the inter-event intervals from a timeseries

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        inter-spike intervals as the second column and the time instances when
        those maximas occur as the third column. If no valid maximas found,
        returns a [par, 0, 0] array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    exevs=spikes(x,t,par)

    shapes=np.shape(exevs)
    if len(shapes)>1 and shapes[0]>1:

        r=np.zeros((len(exevs)-1,3))

        for i in range(len(r)):
            r[i,0]=exevs[i+1,0]
            r[i,1]=exevs[i+1,2]-exevs[i,2]
            r[i,2]=exevs[i+1,2]

        return r

    else:
        print("Not enough Spikes to compute inter-spike intervals")
        return [par,0,0]

def complete_isi(data,time,par):
    """Computes inter-event intervals of multiple time series

    Args:
        data (numpy 2D array)   : Multiple times series showing spikes
        t (numpy array)         : Time instances corresconding to the data.
        par                     : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        overall inter-spike intervals as the second column and the time
        instances when those maximas occur as the third column.

    """

    node_1=data[:,0]
    node_2=data[:,4]

    r1=isi(node_1,time,par)
    r2=isi(node_2,time,par)

    r=np.vstack((r1,r2))
    s=r[r[:,2].argsort()]

    for i in range(len(s)-1):
        s[i+1,1]=s[i+1,2]-s[i,2]

    s=s[1:,:]

    return s

def string_generator(data,time):
    """Generates the spiking string

    Args:
        data (numpy 2D array)   : Multiple times series showing spikes
        t (numpy array)         : Time instances corresconding to the data.

    Returns:
        A string of characters denoting the spikes classified by the source.

    """

    node_1=data[:,0]
    node_2=data[:,4]

    r1=isi(node_1,time,1)
    r2=isi(node_2,time,2)

    r=np.vstack((r1,r2))
    s=r[r[:,2].argsort()]

    string=""
    i=0
    while i<len(s)-1 :
        if s[i,0]==1 :
            string+="0"
        else :
            string+="1"
        i=i+1

    return string

def get_loc(i_grid, i_osc, size_grid, dim_osc):
    """ Convert grid and oscillator index to vector index.

    Args:
        i_grid      : Grid index
        i_osc       : Oscillator index
        size_grid   : Size of the grid
        dim_osc     : Dimensions of each oscillator

    Returns:
        The vector index.

    """

    yield (i_grid*size_grid+i_osc)*dim_osc
    yield (i_grid*size_grid+i_osc)*dim_osc+1

def change_base(number,from_base=3,to_base=10):
    """ Converts a given no. from one base to another.

    Args:
        number      : Number to be converted
        from_base   : Base of the given number
        to_base     : Base to which number has to converted

    Returns:
        The transfomed number.

    """

    n = int(number)

    r=0
    power=0
    while n>0:
        d=n%to_base
        n=int(n/to_base)
        r+=d*(from_base**power)
        power+=1

    return r

def compute_entropy(string,word_length,base):
    """ Comutes entropy of the string wrt a given word length.

    Args:
        string      : The string whose entropy is required
        word_length : The word length
        base        : Base or the no. of letters in the alphabet

    Returns:
        The entropy


    """

    table = np.zeros(base**word_length)
    for i in range(len(string)-word_length+1):
        curr_word=string[i:i+word_length]
        index=change_base(curr_word,base,10)
        table[index]+=1

    table = table[table!=0]
    total = sum(table)
    probab = table/total
    log_probab = np.log(probab)/np.log(base)
    entropy = sum(-log_probab*probab)

    return entropy

def compute_h(s,n,base):
    """ Computes h(n). """

    return compute_entropy(s,n+1,base) - compute_entropy(s,n,base)
