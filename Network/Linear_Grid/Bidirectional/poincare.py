import numpy as np
import matplotlib.pylab as plt


def Poincare(fname,transience,section,plx,ply):

    data = np.loadtxt(fname,skiprows=transience,usecols=(section,plx,ply))
    print("Data Loaded")

    l = len(data)

    for i in range(l-1):
        print(i/l)
        if data[i,0]* data[i+1,0] < 0.0:
            plt.plot(data[i,1],data[i,2],'.b')

    plt.title("Poincare Section at $y_{1,1}=0$")
    plt.xlabel("$x_{1,2}$")
    plt.ylabel("$x_{2,2}$")
    plt.savefig("Poincare_2.png")
    plt.savefig("Poincare_2.pdf")

def Part_Poincare(fname,transience,section,plx,ply):

    data = np.loadtxt(fname,skiprows=transience,usecols=(section,plx,ply))
    print("Data Loaded")

    l = len(data)

    for i in range(l-1):
        print(i/l)
        if data[i,0]* data[i+1,0] < 0.0:
            plt.plot(data[i,1],data[i,2],'.b')

    plt.title("Poincare Section at $y_{1,1}=0$")
    plt.xlabel("$x_{1,2}$")
    plt.ylabel("$x_{2,2}$")
    plt.savefig("Poincare_2.png")
    plt.savefig("Poincare_2.pdf")

if __name__=="__main__" :
    Poincare('timeseries_0.dat',500000,1,2,6)
