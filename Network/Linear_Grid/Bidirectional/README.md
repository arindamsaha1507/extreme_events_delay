# Bidirectional Coupling


The equations

$$ X_{i,j} = F \left( X_{i,j} \right) + \sum_{k,l} M^{int}_{i,j,k,l} \left[
X_{i,k} \left( t-\tau^{int}_{i,j,k,l} \right) - X_{i,j} \right] + \sum_{m,n,l}
M^{ext}_{i,m,l} \left[ X_{m,n} \left( t-\tau^{ext}_{i,m,l} \right) - X_{i,j}
\right] $$

## Default Parameters

Paraamter					                | Variable	    | Value
--------------------------------------------|---------------|----------------------------
No. of layers in intra-node network	    	| n_layers_int	|   2
No. of layers in inter-node network		    | n_layers_ext	|   1
Time delays in intra-node network		    | tau_int	    |   np.array([80, 70])
Coupling strengths in intra-node network 	| M_int		    |   np.array([0.005, 0.0053])
Time delays in inter-node network		    | tau_ext	    |   np.array([1000])
Coupling strengths in inter-node network 	| M_ext		    |   np.array([0.001])
No. of oscillators on each grid point  		| n_osc_int	    |   2
No. of grid points				            | n_grid	    |   2
Internal parameter				            | a 		    |   -0.025
Internal parameter				            | b 		    |   0.00652
Internal parameter				            | c 		    |   0.02

## Dynamical Regimes

Aim is to analyze what new dynamical regimes can be observed

1. **Completely Synchronised Limit Cycle**
    - *Paramters:* M_ext = 0.0005, M_int = [0.05, 0.053], tau_int = [80,70]
    - Uncoupled nodes exhibit extreme events
    - The *entire* system is in synchrony
        - The oscillators in the same node are in synchrony
        - The oscillators in different nodes are also in synchrony

    - **Two Dimensional Plots**

        ![alt text](Figures/Limit_Cycle/Beyond_Transience_1.png) ![alt text](Figures/Limit_Cycle/Beyond_Transience_2.png)
        ![alt text](Figures/Limit_Cycle/Phase_1.png) ![alt text](Figures/Limit_Cycle/Phase_2.png)
        ![alt text](Figures/Limit_Cycle/Synchrony_1.png) ![alt text](Figures/Limit_Cycle/Synchrony_2.png)

        ![alt text](Figures/Limit_Cycle/Inter_Node_Synchrony.png)

2. **Quasiperiodicity**
    - *Parameters:* M_ext = 0.001, M_int = [0.005, 0.0053], tau_int = [80, 70]
    - Uncoupled nodes exhibit extreme events
    - **Aditional Highlights**
        - Significantly long chaotic small amplitude oscillations possible
		- Oscillators in a single node are in synchrony
            - As a result the following 3D plots are planar
                - Any 3 out of the 4 variables in a particular node
                - Any 3 out of the 4  difference variables between the 
                    corresponding variables of two different nodes
            - The 3D plots bear a resemblances from the torus
		- Oscillators on different nodes are not in synchrony
        - The oscillators in the two nodes show two different quasiperiodic
            motions: with two *different* 'amplitudes

    - **Two Dimensional Plots**

        ![alt text](Figures/Quasi/Raw_1.png) ![alt text](Figures/Quasi/Raw_2.png)
        ![alt text](Figures/Quasi/No_Transience_1.png) ![alt text](Figures/Quasi/No_Transience_2.png)
        ![alt text](Figures/Quasi/Synchrony_1.png) ![alt text](Figures/Quasi/Synchrony_2.png)
        ![alt text](Figures/Quasi/Phase_1.png) ![alt text](Figures/Quasi/Phase_2.png)

        ![alt text](Figures/Quasi/Inter_Node_Synchrony.png)

    - **Three Dimensional Plots**

        ![alt text](Figures/Quasi/Projection_1_1.png)
        ![alt text](Figures/Quasi/Projection_1_2.png)

        Other Projections

        ![alt text](Figures/Quasi/Projection_2_1.png)
        ![alt text](Figures/Quasi/Projection_3_1.png)
        ![alt text](Figures/Quasi/Projection_4_1.png)

    - **Poincare Section**

        ![alt text](Figures/Quasi/Poincare_1.png)

4. **Small Amplitude Chaotic**
    - *Parameters:* M_ext = 0.0018, M_int = [0.05, 0.053]
    - Oscillators in a single node are in synchrony
    - Oscillators on different nodes are not in synchrony

    - **Plots**

        ![alt text](Figures/Chaotic/Raw_1.png) ![alt text](Figures/Chaotic/Raw_2.png)

3. **Extremely Rare Extreme Events**
	-  __*Parameters:* M_ext = 0.001, M_int = [0.005, 0.005]__
	- *Parameters:* M_ext = 0.002, M_int = [0.005, 0.0053]
	- ~~Uncoupled nodes show almost periodic events~~ (Not necessarily true)
	- ~~Uncoupled nodes are on the verge of showing extreme events: M_int = [0.005, 0.0053], M_ext = 0 show 'traditional' extreme events~~ (Not necessarily true)
	- Why *extremely rare*?
		- A 'traditional' extreme event occurs approx 1250 times in 5,000,000 time units
		- This extreme event occurs 2-3 times in 5,000,000 time units
	- **Additional Highlights**
		- Shows *pattern-switching* between chaotic and quasiperiodic transiences
		- During quasiperiodic transiences, the two different nodes exhibit two different modes.
		- Oscillators in a single node are in synchrony
		- Oscillators on different nodes are not in synchrony
        - Changing one of the M_int is a very effective way to control the
            'frequency' of extreme events

    - **Two Dimensional Plots**

        ![alt text](Figures/Rare/Raw_1.png) ![alt text](Figures/Rare/Raw_2.png)
        ![alt text](Figures/Rare/Zoomed_1.png) ![alt text](Figures/Rare/Zoomed_2.png)
        ![alt text](Figures/Rare/Phase_1.png) ![alt text](Figures/Rare/Phase_2.png)
        ![alt text](Figures/Rare/Phase_3.png) ![alt text](Figures/Rare/Phase_4.png)
        ![alt text](Figures/Rare/Phase_5.png) ![alt text](Figures/Rare/Phase_6.png)

4. **Extreme Events Type 1**
