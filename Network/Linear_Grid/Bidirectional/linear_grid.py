# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 14:23:33 2017

Linear Grid: Unidirectional

@author: arindam
"""

from __future__ import print_function
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np
import matplotlib.pylab as plt
import functions as funcs
from subprocess import call
import sys

"""
Parameters
"""

n_layers_int=   2
n_layers_ext=   1

tau_int=        np.array([80, 70])
M_int=          np.array([0.005, 0.000])

tau_ext=        np.array([1000])
M_ext=          np.array([0.003])

n_osc_int=      2

n_grid=         2

a =             -0.025
b =              0.00652
c =              0.02

tf = 1000000

"""
File Names
"""

base = "/gss/work/musa2346/FHN/Bursting/Histogram/"

folder_1 = "Data/"
folder_2 = "ISI_Node_1/"
folder_3 = "ISI_Node_2/"
folder_4 = "Complete_ISI/"
folder_5 = "String/"

filename_1 = "timeseries_"
filename_2 = "isi_"
filename_3 = "isi_"
filename_4 = "complete_isi_"
filename_5 = "string_"

extension = ".dat"


"""
DDE Definition
"""

t, y, current_y, past_y, anchors = provide_advanced_symbols()

def f():

    for i in range(n_grid):                                     # for each target grid point

#        print("Accessing grid point {}...".format(i))

        for j in range(n_osc_int):                              # for each target oscillator in the grid point

#           print("Accessing oscillator {}...".format(j))

            x_loc=i*n_osc_int*2+2*j                                  # index of the x co-ordinate of the current oscillator
            y_loc=x_loc+1                                        # index of the x co-ordinate of the current oscillator

#            print("Incorporating internal Dynamics...")
            rhs_x=y(x_loc)*(y(x_loc)-1)*(a-y(x_loc))-y(y_loc)
            rhs_y=b*y(x_loc)-c*y(y_loc)

#            print("Making internal connections...")

            for k in range(n_osc_int):

                if k==j:
                    continue
                else:
                    x_loc_int=i*n_osc_int*2+2*k
                    y_loc_int=x_loc_int+1

                    for l in range(n_layers_int):
                        rhs_x+=M_int[l]*(y(x_loc_int,t-tau_int[l])-y(x_loc))
                        rhs_y+=M_int[l]*(y(y_loc_int,t-tau_int[l])-y(y_loc))

#            print("Making external connections...")

            if i!=0:

                for l in range(n_osc_int):

                    x_loc_ext_backward=(i-1)*n_osc_int*2+2*l
                    y_loc_ext_backward=x_loc_ext_backward+1

                    for m in range(n_layers_ext):

                        rhs_x+=M_ext[m]*(y(x_loc_ext_backward,t-tau_ext[m])-y(x_loc))
                        rhs_y+=M_ext[m]*(y(y_loc_ext_backward,t-tau_ext[m])-y(y_loc))

            if i!=n_grid-1:

                for l in range(n_osc_int):

                    x_loc_ext_forward=(i+1)*n_osc_int*2+2*l
                    y_loc_ext_forward=x_loc_ext_forward+1

                    for m in range(n_layers_ext):

                        rhs_x+=M_ext[m]*(y(x_loc_ext_forward,t-tau_ext[m])-y(x_loc))
                        rhs_y+=M_ext[m]*(y(y_loc_ext_forward,t-tau_ext[m])-y(y_loc))

            yield rhs_x
            yield rhs_y

n_iter = 1

for i in range(n_iter):

    print([i,M_int]);

    DDE = jitcdde(f)

    starting_state=np.random.random(n_grid*n_osc_int*2)

    DDE.add_past_point(-np.max(tau_ext), starting_state, np.zeros(n_grid*n_osc_int*2))
    DDE.add_past_point(0.0,   starting_state, np.zeros(n_grid*n_osc_int*2))

    DDE.generate_f_C()
    DDE.set_integration_parameters()

    pre_iterations = 100
    DDE.integrate_blindly(pre_iterations,1.0)

    dt = 1.0

    values = np.vstack(DDE.integrate(t) for t in
                       np.arange(pre_iterations,pre_iterations+tf,dt))

    np.savetxt("timeseries.dat",values)

#    fname_1 =base+folder_1+filename_1+str(i)+"_"+str(sys.argv[1])+extension
#    fname_2 =base+folder_2+filename_2+str(i)+"_"+str(sys.argv[1])+extension
#    fname_3 =base+folder_3+filename_3+str(i)+"_"+str(sys.argv[1])+extension
#    fname_4 =base+folder_4+filename_4+str(i)+"_"+str(sys.argv[1])+extension
#    fname_5 =base+folder_5+filename_5+str(i)+"_"+str(sys.argv[1])+extension
#
#    np.savetxt(fname_1,values)
#
#    print("Data File written")
#
#    ts=np.arange(len(values[:,0]))
#
#    curr_isi=funcs.isi(values[:,0],ts,M_ext[0])
#    np.savetxt(fname_2,curr_isi)
#    print("Inter-Spike Intervals for the first node computed")
#
#    curr_isi=funcs.isi(values[:,4],ts,M_ext[0])
#    np.savetxt(fname_3,curr_isi)
#    print("Inter-Spike Intervals for the second node computed")
#
#    curr_isi=funcs.complete_isi(values,ts,M_ext[0])
#    np.savetxt(fname_4,curr_isi)
#    print("Complete Inter-Spike Intervals computed")
#
#    sequence=funcs.string_generator(values,ts)
#    string_file = open(fname_5,"w")
#    string_file.write(sequence)
#    string_file.close()
#    print("String Computed")
