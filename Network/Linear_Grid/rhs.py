from jitcdde import provide_advanced_symbols, jitcdde
"""
DDE Definition
"""

t, y, current_y, past_y, anchors = provide_advanced_symbols()

def f():

    for i in range(n_grid):                                     # for each target grid point

#        print("Accessing grid point {}...".format(i))

        for j in range(n_osc_int):                              # for each target oscillator in the grid point

#           print("Accessing oscillator {}...".format(j))

            x_loc=i*n_osc_int*2+2*j                                  # index of the x co-ordinate of the current oscillator
            y_loc=x_loc+1                                        # index of the x co-ordinate of the current oscillator

#            print("Incorporating internal Dynamics...")
            rhs_x=y(x_loc)*(y(x_loc)-1)*(a-y(x_loc))-y(y_loc)
            rhs_y=b*y(x_loc)-c*y(y_loc)

#            print("Making internal connections...")

            for k in range(n_osc_int):

                if k==j:
                    continue
#                    print("Skipping Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(k,i,j,i))
                else:
                    x_loc_int=i*n_osc_int*2+2*k
                    y_loc_int=x_loc_int+1

#                    print("Adding Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(k,i,j,i))
                    for l in range(n_layers_int):
                        rhs_x+=M_int[l]*(y(x_loc_int,t-tau_int[l])-y(x_loc))
                        rhs_y+=M_int[l]*(y(y_loc_int,t-tau_int[l])-y(y_loc))

#            print("Making external connections...")

            if i==0:    # if it is the first grid point, then it is source

#                print("Grid point is a source... only subtracting flow")

                rhs_x-=np.sum(M_ext)*y(x_loc)
                rhs_y-=np.sum(M_ext)*y(y_loc)

            elif i==n_grid-1:

#                print("Grid point is a sink... only adding flow")

                for  l in range(n_osc_int):

                    x_loc_ext=(i-1)*n_osc_int*2+2*l
                    y_loc_ext=x_loc_ext+1

#                    print("Adding Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(l,i-1,j,i))

                    for m in range(n_layers_ext):

                        rhs_x+=M_ext[m]*y(x_loc_ext,t-tau_ext[m])
                        rhs_y+=M_ext[m]*y(y_loc_ext,t-tau_ext[m])
            else:

#                print("Grid Point is neither a source nor a sink")

                for  l in range(n_osc_int):

                    x_loc_ext=(i-1)*n_osc_int*2+2*l
                    y_loc_ext=x_loc_ext+1

#                    print("Adding Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(l,i-1,j,i))

                    for m in range(n_layers_ext):

                        rhs_x+=M_ext[m]*y(x_loc_ext,t-tau_ext[m])
                        rhs_y+=M_ext[m]*y(y_loc_ext,t-tau_ext[m])

#                print("Subtracting flows...")

                rhs_x-=np.sum(M_ext)*y(x_loc)
                rhs_y-=np.sum(M_ext)*y(y_loc)

            yield rhs_x
            yield rhs_y
