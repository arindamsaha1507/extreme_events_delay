# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 14:23:33 2017

Linear Grid: Unidirectional

@author: arindam
"""

from __future__ import print_function
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np
import matplotlib.pylab as plt


"""
Parameters
"""

n_layers_int=   2
n_layers_ext=   1

tau_int=        np.array([80, 70])
M_int=          np.array([0.005, 0.0053])

tau_ext=        np.array([1000])
M_ext=          np.array([0])

n_osc_int=      2

n_grid=         2

a =             -0.025
b =              0.00652
c =              0.02


"""
DDE Definition
"""

#, y, current_y, past_y, anchors = provide_advanced_symbols()

#def f():

for i in range(n_grid):                                     # for each target grid point

        print("Accessing grid point {}...".format(i))

        for j in range(n_osc_int):                              # for each target oscillator in the grid point

            print("Accessing oscillator {}...".format(j))

#           x_loc=i*n_grid+2*j                                  # index of the x co-ordinate of the current oscillator
#           y_loc=xloc+1                                        # index of the x co-ordinate of the current oscillator

            print("Incorporating internal Dynamics...")
#           rhs_x=y(x_loc)*(y(x_loc)-1)*(a-y(x_loc))-y(y_loc)
#           rhs_y=b*y(x_loc)-c*y(y_loc)

            print("Making internal connections...")

            for k in range(n_osc_int):

                if k==j:
                    print("Skipping Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(j,i,k,i))
                else:
#                   x_loc_int=i*n_grid+2*k
#                   y_loc_int=x_loc_int+1

                    print("Adding Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(j,i,k,i))
#                   for l in range(n_layers_int):
#                       rhs_x+=M_int[l]*(y(x_loc_int,t-tau_int[l])-y(x_loc))
#                       rhs_y+=M_int[l]*(y(y_loc_int,t-tau_int[l])-y(y_loc))

            print("Making external connections...")

            if i==0:    # if it is the first grid point, then it is source

                print("Grid point is a source... only subtracting flow")

#               rhs_x-=np.sum(M_ext)*y(x_loc)
#               rhs_y-=np.sum(M_ext)*y(y_loc)

            elif i==n_grid-1:

                print("Grid point is a sink... only adding flow")

                for  l in range(n_osc_int):

#                   x_loc_ext=(i-1)*n_grid+2*l
#                   y_loc_ext=x_loc_ext+1

                    print("Adding Connection from Oscillator {} of Grid Point {} to Oscillator {} of Grid Point {} ".format(l,i-1,j,i))

#                   for m in range(n_layers_ext):

#                       rhs_x+=M_ext[m]*y(x_loc_ext,t-tau_ext[m])
#                       rhs_y+=M_ext[m]*y(y_loc_ext,t-tau_ext[m])

            print()

        print()

print("Done")



