import numpy as np

"""
Parameters
"""

n_layers_int=   2
n_layers_ext=   1

tau_int=        np.array([80, 70])
M_int=          np.array([0.005, 0.0055])

tau_ext=        np.array([1000])
M_ext=          np.array([0.001])

n_osc_int=      2

n_grid=         3

a =             -0.025
b =              0.00652
c =              0.02


