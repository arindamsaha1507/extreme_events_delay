def get_loc(i_grid, i_osc, size_grid, dim_osc):
    """ Convert grid and oscillator index to vector index. """
    yield (i_grid*size_grid+i_osc)*dim_osc
    yield (i_grid*size_grid+i_osc)*dim_osc+1
