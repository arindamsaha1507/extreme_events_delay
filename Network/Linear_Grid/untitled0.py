# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 14:23:33 2017

Linear Grid: Unidirectional

@author: arindam
"""

from __future__ import print_function
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np
import matplotlib.pylab as plt


"""
Parameters
"""

n_layers_int=2
n_layers_ext=1

tau_int=np.array([80, 70])
M_int=np.array([0.005, 0.0053])

tau_ext=np.array([1000])
M_ext=np.array([0])

n_osc_int=2

n_grid=2

"""
DDE Definition
"""

