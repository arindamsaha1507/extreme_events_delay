# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 08:03:45 2016

@author: arindam
"""

from __future__ import print_function
import matplotlib.pylab as plt
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np

"""
Parameters
"""

a = -0.025
b =  0.00652
c =  0.02

n_osc=3
n_layers=2

M_net=np.empty((n_layers,n_osc,n_osc))
tau_net=np.empty((n_layers,n_osc,n_osc))

t, y, current_y, past_y, anchors = provide_advanced_symbols()

"""
Ring Network (Uniform)
"""

M=np.array([0.01/n_osc,0.0075/n_osc])
tau=np.array([80.0,70.0])

for i in range(n_layers):
    for j in range(n_osc):
        for k in range(n_osc):
            if k==j+1:
                M_net[i,j,k]=M[i]
            elif k==j-1:
                M_net[i,j,k]=M[i]
            elif j==0 and k==n_osc-1:
                M_net[i,j,k]=M[i]
            elif j==n_osc-1 and k==0:
                M_net[i,j,k]=M[i]
            else:
                M_net[i,j,k]=0

for i in range(n_layers):
    for j in range(n_osc):
        for k in range(n_osc):
            if k==j+1:
                tau_net[i,j,k]=tau[i]
            elif k==j-1:
                tau_net[i,j,k]=tau[i]
            elif j==0 and k==n_osc-1:
                tau_net[i,j,k]=tau[i]
            elif j==n_osc-1 and k==0:
                tau_net[i,j,k]=tau[i]
            else:
                tau_net[i,j,k]=0

"""
The RHS
"""

def f():

    for curr_osc in range(n_osc):

        x_dim=y(2*curr_osc)*(y(2*curr_osc)-1)*(a-y(2*curr_osc))-y(2*curr_osc+1)
        y_dim=b*y(2*curr_osc)-c*y(2*curr_osc+1)

        for src_osc in range(n_osc):
            for layer in range(n_layers):
                x_dim+=M_net[layer,curr_osc,src_osc]*(y(2*src_osc,t-tau_net[layer,curr_osc,src_osc])-y(2*curr_osc))
                y_dim+=M_net[layer,curr_osc,src_osc]*(y(2*src_osc+1,t-tau_net[layer,curr_osc,src_osc])-y(2*curr_osc+1))

        yield x_dim
        yield y_dim

"""
Core Program
"""

DDE = jitcdde(f)

starting_state = np.random.random(2*n_osc)
DDE.add_past_point(-tau[0], starting_state, np.zeros(2*n_osc))
DDE.add_past_point(0.0,   starting_state, np.zeros(2*n_osc))

DDE.generate_f_c()
DDE.set_integration_parameters()

pre_iterations = 100
DDE.integrate_blindly(pre_iterations,1.0)
#DDE.integrate(1000.0)

dt = 1.0

values = np.vstack(DDE.integrate(t) for t in np.arange(pre_iterations,pre_iterations+100000,dt))

#values = np.vstack(DDE.integrate(t))
#for t in np.arange(pre_iterations,pre_iterations+100000,dt):
#    v1=DDE.integrate(t)
#    values = np.vstack((values,v1))
#    print(t)

np.savetxt("timeseries.dat",values)

for i in range(n_osc):
    plt.plot(range(len(values[:,0])),values[:,2*i])
