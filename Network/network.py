"""
Created by Arindam Saha

"""

import numpy as np
import networkx as nx

class Network:

    def __init__(self, size=0, typ=0, par=None):

        if size == 0:
            self.graph = nx.Graph()
            return

        if typ == 1:         # Watts Strogartz Small World
            n_neighbours = par[0]
            prob = par[1]
            self.graph = nx.watts_strogatz_graph(size, n_neighbours, prob)
        elif typ == 2:      # Barabasi Albert Scale Free
            seed_size = par[0]
            self.graph = nx.barabasi_albert_graph(size, seed_size)
        elif typ == 3:      # Complete
            self.graph = nx.complete_graph(size)
        elif typ == 4:      # Ring
            self.graph = nx.cycle_graph(size)
        else:               # Empty graphraph with n nodes and no edges
            self.graph = nx.empty_graph(size)

        self.adj_mat = nx.to_numpy_matrix(self.graph)
        self.n_nodes = size
