import numpy as np

class global_parameters:

    def __init__(self):

        # FHN Internal Parameters

        self.a = -0.025
        self.b =  0.00652
        self.c =  0.02

        # Intra-node Coupling Paramters

        self.n_intra_osc     = 2
        self.n_intra_layers  = 2

        # Inter-node Coupling Paramters

        self.n_inter_nodes   = 2
        self.n_inter_layers  = 1

        # Integration Parameters

        self.tf                  = 1000000
        self.dt                  = 1.0
        self.transience          = 500000

        # File Names

        self.ts_name             = "timeseries.dat"
        self.fi_name             = "data.dat"

        # Confirmation Message

        print("Global Paramters Set")
