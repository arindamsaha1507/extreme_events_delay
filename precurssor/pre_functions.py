import numpy as np

def maxima(x,t,par):
    """Detects maximas in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas as the second column and the time instances when those maximas
        occur as the third column. If no maximas found, returns a [par, 0, 0]
        array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    m=np.array([par,0,0])
    for i in range(1,len(x)-1):
        if x[i]>x[i+1] and x[i]>x[i-1]:
            m=np.vstack((m,np.array([par,x[i],t[i]])))
    if np.shape(m)==(3,):
        print("No maximas detected")
    else:
        m=m[1:,:]
    return m

def spikes(x,t,par,threshold=0.9):
    """Detects maximas above a set threshold (spikes) in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.
        threshold       : Theshold above which the maximas are defined spikes.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas above the threshold as the second column and the time instances
        when those maximas occur as the third column. If no valid maximas found,
        returns a [par, 0, 0] array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    e=np.array([par,0,0])
    maxs=maxima(x,t,par)

    if np.shape(maxs)==(3,):
        print("No Spikes detected")
        return e

    for i in range(len(maxs)):
        if maxs[i,1]>threshold:
            e=np.vstack((e,maxs[i,:]))

    if np.shape(e)==(3,):
        print("No Spikes detected")
    else:
        e=e[1:,:]
    return e

def precursor(x,t,threshold=0.001):

    theta1=np.arctan(x[:,1]/x[:,0])
    theta2=np.arctan(x[:,3]/x[:,2])
    pre=np.sin(theta1-theta2)

    r=[0]

    i=0
    while i<len(x):
        if abs(pre[i])>0.1:
            r=np.append(r,t[i])
            i=i+4000
        i=i+1

    return r
