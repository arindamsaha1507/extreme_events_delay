# -*- coding: utf-8 -*-
"""
Created on Sat Feb  11 11:24:50 2017

@author: arindam
"""
import numpy as np
import matplotlib.pylab as plt

def determine_markers(x,t,tr=1000,exev_threshold=0.2,sync_threshold=0.1):
    """Finds out markers indicating the start and the end of extreme events

    Args:
        x (numpy array) : Data where extreme events are to be detected
        t (numpy array) : Time instances corresponding to the data.
        tr              : Transience: first tr entries of x will be ignored.
        exev_threshold  : Threshold amplitude for defining extreme event.
        sync_threshold  : Maximum allowed seperation for defining synchrony.

    Returns:
        A four column matrix with each row of the form [osc_no, start_time,
        end_time, type] where osc_no is the oscillator no. showing the event
        from start_time to end_time. The type is indicated as either 1 for
        a synchronous extreme event, 2 for a asynchronous extreme event and 3
        for stabilization to a fixed point.

    Remarks:
        For this study, extreme events are defined to be oscillations with a
        maxima above a threshhold and oscillations are difined to start
        whenever the data crosses zero and has a positive slope.

    """

    foo = open("temp.dat","w")

    for k in range(2) :

        data1 = x[:,k*2]                # Reference data
        data2 = x[:, np.mod(k*2+2,4)]   # Auxilliary data

        i            = tr
        i_start      = tr

        flag         = 0

        if data1[i]>0 :
            cross = 0
            check = 1
        else :
            cross = 1
            check = -1

        l = len(t)

        while i <  l-1 :

             if np.sign(data1[i]) == check :
                 i = i + 1
             else :
                 check = np.sign(data1[i])
                 if cross == 0 :
                     cross = 1
                 else :
                     cross = 0
                     i_end = i
                     if max(data1[i_start:i_end]) < exev_threshold :
                         pass
                     elif abs(max(data1[i_start:i_end] - data2[i_start:i_end])) < sync_threshold :
                         foo.write("{}\t{}\t{}\t{}\n".format(k, i_start, i_end, 1))
                     else :
                         foo.write("{}\t{}\t{}\t{}\n".format(k, i_start, i_end, 2))
                     i_start = i_end

        if t[i-1]>t[i_start]+500 :
             i_end = i
             foo.write("{}\t{}\t{}\t{}\n".format(k, i_start, i_end, 3))

    foo.close()

    print("Markers Determined")

    markers = np.loadtxt("temp.dat")
    markers = markers.astype(int)

    return markers

def color_timeseries(x,t,fname="Plot_timeseries.pdf"):
    """Plots the timeseries colored according to the type of extreme event

    Args:
        x (numpy array) : Data where extreme events are to be plotted.
        t (numpy array) : Time instances corresponding to the data.
        fname           : Name of the output file

    Remarks:
        Non-extreme events marked in green.
        Synchronous extreme events marked in blue.
        Asynchronous extreme events marked in red.

    """

    markers = determine_markers(x,t)

    tracker = 0

    plt.figure(figsize=(8,5))

    for i in range(len(markers)) :

        p1 = tracker
        p2 = markers[i, 1]
        p3 = markers[i, 2]

        if markers[i, 0] == 0 :

            plt.plot(t[p1:p2], x[p1:p2, 0], 'g')
            if markers[i, 3] == 1 :
                plt.plot(t[p2:p3], x[p2:p3, 0], 'b')
            elif markers [i, 3] == 2 :
                plt.plot(t[p2:p3], x[p2:p3, 0], 'r')
            else :
                plt.plot(t[p2:p3], x[p2:p3, 0], 'm')
            tracker = markers[i, 2]

        else :
            plt.plot(t[p1:p2], x[p1:p2, 2], 'g')
            if markers[i, 3] == 1 :
                plt.plot(t[p2:p3], x[p2:p3, 2], 'b')
            elif markers[i, 3] == 2 :
                plt.plot(t[p2:p3], x[p2:p3, 2], 'r')
            else :
                plt.plot(t[p2:p3], x[p2:p3, 2], 'm')
            tracker = markers[i, 2]

    plt.xlabel("Time",fontsize=16)
    plt.ylabel("$x_i$",fontsize=16)

    print("Plotted")
    plt.savefig(fname)
