# -*- coding: utf-8 -*-
"""
Created on Sun Sep  4 07:16:32 2016

@author: arindam
"""
import numpy as np

def maxima(x,t,par):
    """Detects maximas in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas as the second column and the time instances when those maximas
        occur as the third column. If no maximas found, returns a [par, 0, 0]
        array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    m=np.array([par,0,0])
    for i in range(1,len(x)-1):
        if x[i]>x[i+1] and x[i]>x[i-1]:
            m=np.vstack((m,np.array([par,x[i],t[i]])))
    if np.shape(m)==(3,):
        print("No maximas detected")
    else:
        m=m[1:,:]
    return m

def spikes(x,t,par,threshold=0.9):
    """Detects maximas above a set threshold (spikes) in a given timeseries.

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.
        threshold       : Theshold above which the maximas are defined spikes.

    Returns:
        A three column matrix containing the parameter as the first column, the
        maximas above the threshold as the second column and the time instances
        when those maximas occur as the third column. If no valid maximas found,
        returns a [par, 0, 0] array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    e=np.array([par,0,0])
    maxs=maxima(x,t,par)

    if np.shape(maxs)==(3,):
        print("No Spikes detected")
        return e

    for i in range(len(maxs)):
        if maxs[i,1]>threshold:
            e=np.vstack((e,maxs[i,:]))

    if np.shape(e)==(3,):
        print("No Spikes detected")
    else:
        e=e[1:,:]
    return e


def isi(x,t,par):
    """Computes the inter-event intervals from a timeseries

    Args:
        x (numpy array) : Data whose maximas are to be detected.
        t (numpy array) : Time instances corresconding to the data.
        par             : Parameter at which the timeseries was created.

    Returns:
        A three column matrix containing the parameter as the first column, the
        inter-spike intervals as the second column and the time instances when
        those maximas occur as the third column. If no valid maximas found,
        returns a [par, 0, 0] array with a warning.

    Remarks:
        * Length of arguments x and t must be equal.
        * Argument par is provided primarily to track the parameter for
          making bifurcation diagrams.

    """

    exevs=spikes(x,t,par)

    shapes=np.shape(exevs)
    if len(shapes)>1 and shapes[0]>1:

        r=np.zeros((len(exevs)-1,3))

        for i in range(len(r)):
            r[i,0]=exevs[i+1,0]
            r[i,1]=exevs[i+1,2]-exevs[i,2]
            r[i,2]=exevs[i+1,2]

        return r

    else:
        print("Not enough Spikes to compute inter-spike intervals")
        return [par,0,0]
