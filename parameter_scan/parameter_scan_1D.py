# -*- coding: utf-8 -*-
"""
Created on Mon Sep  5 06:35:55 2016

@author: arindam
"""

from __future__ import print_function
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np
import functions as funcs

# Default parameters

tau1 = 80.0
tau2 =70.0
M1 = 0.005
M2 = 0.0053

a = -0.025
b =  0.00652
c =  0.02

# Setting up symbols

t, y, current_y, past_y, anchors = provide_advanced_symbols()

# Defining the dynamics

def f():
	yield y(0)*(y(0)-1)*(a-y(0)) -y(1) + M1*(y(2,t-tau1)-y(0)) + M2*(y(2,t-tau2)-y(0))
	yield b*y(0)-c*y(1)                + M1*(y(3,t-tau1)-y(1)) + M2*(y(3,t-tau2)-y(1))
	yield y(2)*(y(2)-1)*(a-y(2)) -y(3) + M1*(y(0,t-tau1)-y(2)) + M2*(y(0,t-tau2)-y(2))
	yield b*y(2)-c*y(3)                + M1*(y(1,t-tau1)-y(3)) + M2*(y(1,t-tau2)-y(3))

# The scanning parameter range

par_range =np.linspace(0.0,0.01,10)

# Initialising the data to be recorded

data=np.array([0.0,0.0,0.0])

# The scan

for par in par_range:

    # Assigning the parameter
    M2 = par
    print(par)

    trial=0
    curr_data=np.array([0.0,0.0,0.0])

    while trial<11 and (np.shape(curr_data)==(3,)):
    # The initial condition chosen may converge to a low amplitude limit
    # cycle... This will result in curr_data is a 1x3 mmatrix... Hence
    # try with another initial condition... Upper limit of no. of trials set to
    # 11

        trial=trial+1
        print(trial)

        DDE = jitcdde(f)
        starting_state = np.random.random(4)
        starting_state[2]=starting_state[0]
        starting_state[3]=starting_state[1]
        DDE.add_past_point(-tau1, starting_state, np.zeros(4))
        DDE.add_past_point(0.0,   starting_state, np.zeros(4))

        DDE.generate_f_C()
        DDE.set_integration_parameters()

        pre_iterations = 100
        DDE.integrate_blindly(pre_iterations,1.0)

        dt = 1.0

        values = np.vstack(DDE.integrate(t) for t in
                           np.arange(pre_iterations,pre_iterations+1000000,dt))

        ts=np.arange(len(values[:,0]))
        np.savetxt("timeseries.dat",values)

        # Set the data to be recorded

        curr_data=funcs.isi(values[19000:,0],ts[19000:],par)        # ISI
#        curr_data=funcs.maxima(values[19000:,0],ts[19000:],par)    # Maximas

    data=np.vstack((data,curr_data))

# Remove the first row as it was just for initialization
data=data[1:,:]

# Save data

np.savetxt("Scan_data_1D.dat",data)
