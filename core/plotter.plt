set terminal png giant size 1600, 1200

set output 'Time_Series.png'
plot 'timeseries.dat' u 0:1 w l, 'timeseries.dat' u 0:3 w l

set output 'Difference_Series.png'
plot 'timeseries.dat' u 0:($1-$3) w l

set output 'Difference_Phase.png'
plot 'timeseries.dat' u ($1-$3):($2-$4) w l

set output 'Difference_Sum_x.png'
plot 'timeseries.dat' u ($1-$3):($1+$3) w l

set output 'Difference_Sum_y.png'
plot 'timeseries.dat' u ($2-$4):($2+$4) w l

set output 'Phase_Series.png'
plot 'timeseries.dat' u 0:(sin(atan($2/$1)-atan($4/$3))) w l
