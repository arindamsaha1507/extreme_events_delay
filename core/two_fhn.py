from __future__ import print_function
import matplotlib.pylab as plt
from jitcdde import provide_advanced_symbols, jitcdde
import numpy as np
from subprocess import call

# Parameters

tau1 = 80.0
tau2 = 70.0 #72.0 #69.4
M1 = 0.000
M2 = 0.0100  #0.00255 #0.0075 #0.002559

a = -0.025
b =  0.00652
c =  0.02

# Declaring symbols

t, y, current_y, past_y, anchors = provide_advanced_symbols()

# Defining the Dynamics

def f():
	yield y(0)*(y(0)-1)*(a-y(0)) -y(1) + M1*(y(2,t-tau1)-y(0)) + M2*(y(2,t-tau2)-y(0))
	yield b*y(0)-c*y(1)                + M1*(y(3,t-tau1)-y(1)) + M2*(y(3,t-tau2)-y(1))
	yield y(2)*(y(2)-1)*(a-y(2)) -y(3) + M1*(y(0,t-tau1)-y(2)) + M2*(y(0,t-tau2)-y(2))
	yield b*y(2)-c*y(3)                + M1*(y(1,t-tau1)-y(3)) + M2*(y(1,t-tau2)-y(3))

tf=2000000

# Initializing the system

DDE = jitcdde(f)

# Defining initial state

# starting_state=np.array([0.01, 0.01, 0.01, 0.01])   # Fixed

starting_state = np.random.random(4)              # Random

# starting_state[2]=starting_state[0]               # Random but Synchronized
# starting_state[3]=starting_state[1]               # Random but Synchronized

# Adding History

DDE.add_past_point(-tau1, starting_state, np.zeros(4))
DDE.add_past_point(0.0,   starting_state, np.zeros(4))

# Setting up...

DDE.generate_f_C()
DDE.set_integration_parameters()

pre_iterations = 100
DDE.integrate_blindly(pre_iterations,1.0)

dt = 1.0                                            # Time step for the output

# Integration

values = np.vstack(DDE.integrate(t) for t in np.arange( pre_iterations,
                                                       pre_iterations+tf,dt))

# Writikng on the disk

np.savetxt("timeseries.dat",values)

print("Data File written")

# Plotting

call(["gnuplot","plotter.plt"])
print("Plot Command Given")
