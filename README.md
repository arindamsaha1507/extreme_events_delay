# Generating delay induced extreme events

Scripts for generating and analyzing extreme events in a pair of FitzHugh-Nagumo oscillators coupled using **time delays**.
Also contains some general purpose functions.

*This repository contains the scripts used by Arindam Saha for his PhD.*

Contact:
arindamsaha1507@gmail.com
